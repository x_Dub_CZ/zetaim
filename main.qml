import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3


ApplicationWindow {
    id: window
    visible: true
    width: Qt.application.font.pixelSize * 25
    height: Qt.application.font.pixelSize * 45
    readonly property int responsiveWidth: Qt.application.font.pixelSize * 75
    title: qsTr("ZetaIM")



    SwipeView {
        id: swipeView
                currentIndex: 0
               // currentIndex: Prop.tab
                interactive: false
                anchors.fill: parent
                states: [
                    State {
                        when: window.width >= responsiveWidth
                        ParentChange { target: page1; parent: bodyContainer; }
                        ParentChange { target: page2; parent: body2Container; }
                        //PropertyChanges { target: backButton; visible: false }
                    },
                    State {
                       // when: Prop.tab == 1
                       // PropertyChanges { target: body2.push(Prop.pageb2) }
                    }

                ]
                Item {
                    Rectangle {
                        id: page1
                        anchors.fill: parent
                    ToolBar {
                        id: tb1
                        anchors {
                        top: parent.top
                        right: parent.right
                        left: parent.left
                        }
                        background: Rectangle {
                            id: r
                            anchors.verticalCenter: tb1.verticalCenter
                            implicitHeight: Qt.application.font.pixelSize * 2.8
                            gradient: Gradient{
                                GradientStop{color: "mediumpurple" ; position: 0}
                            }
                        }
                        RowLayout {
                            height: Qt.application.font.pixelSize * 2.5
                            anchors.left: parent.left
                            anchors.leftMargin: 0
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.right: parent.right
                            anchors.rightMargin: 0
                            ToolButton {
                                id: menuButton
                                anchors.left: parent.left
                                implicitHeight: Qt.application.font.pixelSize * 3.2
                                implicitWidth: Qt.application.font.pixelSize * 3.2
                                background: Rectangle {
                                    color: "transparent"
                                }
                                Image {
                                    id: menupic
                                    x: Qt.application.font.pixelSize * 0.4
                                    height: Qt.application.font.pixelSize * 2.6
                                    width: Qt.application.font.pixelSize * 2.6
                                    source: "./resources/images/ui/menu-icon.svg"
                                }
                                font.pixelSize: Qt.application.font.pixelSize * 2.6
                                onClicked: menu.open()


                        Menu {
                            id: menu
                            y: menuButton.height - Qt.application.font.pixelSize * 0.45
                            implicitWidth: Qt.application.font.pixelSize * 10
                            bottomMargin: 0

                            MenuItem {
                                height: Qt.application.font.pixelSize * 2.6
                                Image {
                                    id: accpic
                                    y: Qt.application.font.pixelSize * 0.4
                                    x: Qt.application.font.pixelSize * 0.4
                                    height: Qt.application.font.pixelSize * 2.4
                                    width: Qt.application.font.pixelSize * 2.4
                                    source: "./resources/images/ui/account-icon.svg"
                                }
                                Text {
                                    y: Qt.application.font.pixelSize * 0.8
                                    anchors.left: accpic.right
                                    text: " Spravovat účty"
                                    font.pixelSize: Qt.application.font.pixelSize * 1
                                }
                                onTriggered: {
                                    body2.push("accountslist.qml")
                                }
                            }
                            MenuSeparator {
                                    padding: 1
                                    contentItem: Rectangle {
                                        implicitWidth: 200
                                        implicitHeight: 1
                                        color: "#1E000000"
                                    }
                            }
                            MenuItem {
                                height: Qt.application.font.pixelSize * 2.6
                                Image {
                                    id: pluginpic
                                    y: Qt.application.font.pixelSize * 0.4
                                    x: Qt.application.font.pixelSize * 0.4
                                    height: Qt.application.font.pixelSize * 2.4
                                    width: Qt.application.font.pixelSize * 2.4
                                    source: "./resources/images/ui/plugin-icon.svg"
                                }
                                Text {
                                    y: Qt.application.font.pixelSize * 0.8
                                    anchors.left: pluginpic.right
                                    text: " Doplňky"
                                    font.pixelSize: Qt.application.font.pixelSize * 1
                                }

                            }
                            MenuSeparator {
                                    padding: 1
                                    contentItem: Rectangle {
                                        implicitWidth: 200
                                        implicitHeight: 1
                                        color: "#1E000000"
                                    }
                            }
                            MenuItem {
                                height: Qt.application.font.pixelSize * 2.6
                                Image {
                                    id: settingspic
                                    y: Qt.application.font.pixelSize * 0.4
                                    x: Qt.application.font.pixelSize * 0.4
                                    height: Qt.application.font.pixelSize * 2.4
                                    width: Qt.application.font.pixelSize * 2.4
                                    source: "./resources/images/ui/settings-icon.svg"
                                }
                                Text {
                                    y: Qt.application.font.pixelSize * 0.8
                                    anchors.left: settingspic.right
                                    text: " Nastavení"
                                    font.pixelSize: Qt.application.font.pixelSize * 1
                                }

                            }
                            MenuSeparator {
                                    padding: 1
                                    contentItem: Rectangle {
                                        implicitWidth: 200
                                        implicitHeight: 1
                                        color: "#1E000000"
                                    }
                            }
                        }
                    }
                        Label {
                            text: "<font color='black'>" + body.currentItem.title + "</font>"
                            anchors.top: parent.top
                            elide: Label.ElideRight
                            horizontalAlignment: Qt.AlignHCenter
                            verticalAlignment: Qt.AlignVCenter
                            font.pixelSize: Qt.application.font.pixelSize * 1.8
                            font.bold: true
                            Layout.fillWidth: true
                                }

                    }
                    }
                    StackView {
                        id: body
                        initialItem: "buddylist.qml"
                        anchors {
                        top: tb1.bottom
                        right: parent.right
                        left: parent.left
                        bottom: parent.bottom
                        }
                    }
                }
                }
                Item {
                    Rectangle {
                        id: page2
                        anchors.fill: parent
                        ToolBar {
                            id: tb2
                            anchors {
                            top: parent.top
                            right: parent.right
                            left: parent.left
                            }
                            background: Rectangle {
                                id: r2
                                anchors.verticalCenter: tb2.verticalCenter
                                implicitHeight: Qt.application.font.pixelSize * 2.5
                                gradient: Gradient{
                                    GradientStop{color: "mediumpurple" ; position: 0}
                                }
                            }
                            RowLayout {
                                height: Qt.application.font.pixelSize * 2.6
                                anchors.left: parent.left
                                anchors.leftMargin: 0
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.right: parent.right
                                anchors.rightMargin: 0


                            Label {
                                text: "<font color='black'>" + body2.currentItem.title + "</font>"
                                anchors.top: parent.top
                                anchors.topMargin: Qt.application.font.pixelSize * 0.5
                                elide: Label.ElideRight
                                horizontalAlignment: Qt.AlignHCenter
                                font.pixelSize: Qt.application.font.pixelSize * 1.8
                                font.bold: true
                                Layout.fillWidth: true
                                    }


                        }
                        }
                        StackView {
                            id: body2
                            initialItem: "chat.qml"
                            anchors {
                            top: tb2.bottom
                            right: parent.right
                            left: parent.left
                            bottom: parent.bottom
                            }
                        }
                    }
                }
    }


        Row {
            id: splitView
            anchors.fill: parent
            Item {
                id: bodyContainer
                width: 400 ; height: parent.height
            }
            Item {
                id: body2Container
                width: parent.width - 400; height: parent.height
            }
        }
}
